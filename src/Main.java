import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    /**
     * Программа RegularExpressions
     * Осуществляет поиск идентификаторов и их запись в txt-файл
     * Используются следующие методы:
     * - writeIdentifiersToFile(запись идентификаторов из ArrayList-а в файл)
     * - findingIdentifiers(нахождение идентификаторов)
     * - linesScan(метод, осуществляющий перебор строк и их обработку)
     * - skipMultiLineComment(метод, осуществляющий пропуск многострочного комментария)
     *
     * @param args
     */
    public static void main(String[] args) {
        try (BufferedReader javaclassReader = new BufferedReader(new FileReader("src\\files\\Javaclass.java"))) {

            ArrayList<String> identifiers;

//            identifiers = findingIdentifiers(javaclassReader)
            identifiers = linesScan(javaclassReader);
            writeIdentifiersToFile(identifiers);

        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Метод осуществляет запись идентификаторов из переданного в параметры ArrayList-а
     * Запись в файл "identifiers.txt"
     *
     * @param identifiersArrayList
     * @author Isheykin A.A.
     */
    private static void writeIdentifiersToFile(ArrayList<String> identifiersArrayList) {
        try (BufferedWriter txtfileWriter = new BufferedWriter(new FileWriter("src\\files\\identifiers.txt"))) {
            for (String word : identifiersArrayList) {
                txtfileWriter.write(word + "\n");
            }
        } catch (IOException io_exception) {
            System.out.println("Обнаружен " + io_exception.getMessage() + "!");
        }
    }


    /**
     * Метод осуществляет перебор строк и их обработку
     *
     * @param javaClassReader
     * @return
     * @throws IOException
     * @author Isheykin A.A.
     */
    private static ArrayList<String> linesScan(BufferedReader javaClassReader) throws IOException {
        String currentLine;

        Pattern identifierPattern = Pattern.compile("([A-Za-z_])(\\w)+");
        Pattern singleCommentPattern = Pattern.compile("([ ])+(//)(\\w)*");
        Pattern multiLineCommentPattern = Pattern.compile("([/*][/**][*/][ *])(\\w)*");

        ArrayList<String> identifiers = new ArrayList<>();
        Matcher singleCommentMatcher;
        Matcher identifierMatcher;
        Matcher multiLineCommentMatcher;

        while ((currentLine = javaClassReader.readLine()) != null) {

            singleCommentMatcher = singleCommentPattern.matcher(currentLine);
            identifierMatcher = identifierPattern.matcher(currentLine);
            multiLineCommentMatcher = multiLineCommentPattern.matcher(currentLine);

            if (multiLineCommentMatcher.find()) {
                skipMultiLineComment(javaClassReader, multiLineCommentMatcher, multiLineCommentPattern);
            }
            if (singleCommentMatcher.find()) {
                continue;
            }
            while (identifierMatcher.find()) {
                identifiers.add(identifierMatcher.group());
            }
        }
        return identifiers;
    }

    /**
     * Метод осуществляет пропуск многострочного комментария
     *
     * @param javaClassReader
     * @param multiLineCommentMatcher
     * @param multiLineCommentPattren
     * @throws IOException
     * @author Isheykin A.A.
     */
    private static void skipMultiLineComment(BufferedReader javaClassReader, Matcher multiLineCommentMatcher, Pattern multiLineCommentPattren) throws IOException {
        String currentLine;
        while (multiLineCommentMatcher.find()) {
            try {
                currentLine = javaClassReader.readLine();
                multiLineCommentMatcher = multiLineCommentPattren.matcher(currentLine);
            } catch (IOException ioexception) {
                ioexception.printStackTrace();
            }
        }
    }
}
